export class SerialChartData {
  export: object;
  constructor(
    public dataProvider = [{
      'country': 'USA',
      'visits': 3025,
      'color': '#FF0F00'
    }, {
      'country': 'China',
      'visits': 1882,
      'color': '#FF6600'
    }],
    public graphs = [{
      'balloonText': '<b>[[category]]: [[value]]</b>',
      'fillColorsField': 'color',
      'fillAlphas': 0.9,
      'lineAlpha': 0.2,
      'type': 'column',
      'valueField': 'visits'
    }],
    public chartCursor = {
      'categoryBalloonEnabled': false,
      'cursorAlpha': 0,
      'zoomable': false
    },
    public categoryField = 'country',
    public categoryAxis = {
      'gridPosition': 'start',
      'labelRotation': 45
    },
    public theme = 'light',
    public marginRight = 70,
    public startDuration = 1,
    public type = 'serial',
  ) {

  }
}


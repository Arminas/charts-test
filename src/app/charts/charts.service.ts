import { Injectable } from '@angular/core';
import * as _ from 'underscore';
import { SerialChartData } from './chartsData.module';

@Injectable()

export class ChartService {

  private numberOfCharts = 5;
  public chartsArray: Array<{data: SerialChartData, iteration: number, dataChange: number}>;
  constructor() {}

  setNumberOfChartsToGenerate(number: number): void {
    this.numberOfCharts = number;
    this.createDummyDataForCharts();
  }

  getCharts(): Array<{data: SerialChartData, iteration: number}> {
    return this.chartsArray;
  }

  createDummyDataForCharts(): void {
    this.chartsArray = [];
    for (let i = 0; i < this.numberOfCharts; i++) {
      const dummyData = new SerialChartData();
      this.chartsArray.push({'data': dummyData, 'iteration': i, 'dataChange': 0});
    }
  }

  changeFirstChartData(testNumber: number, iteration: number): void {
    this.chartsArray[iteration].data.dataProvider[0].visits = testNumber;
    this.chartsArray[iteration].dataChange++;
  }

}

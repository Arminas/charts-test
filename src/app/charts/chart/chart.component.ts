import { AfterViewInit, Component, Input, OnChanges, OnDestroy, OnInit, Output } from '@angular/core';
import { AmChartsService, AmChart } from '@amcharts/amcharts3-angular';
import { SerialChartData } from '../chartsData.module';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.less']
})
export class ChartComponent implements OnInit, AfterViewInit, OnDestroy, OnChanges {

  @Input() iteration: number;
  @Input() chartData: SerialChartData;
  @Input() chartDataChange: number;

  private chart: AmChart;
  private previous: any;

  constructor(private AmCharts: AmChartsService) {}

  ngOnInit() {
    this.previous = this.chartData;
  }

  ngAfterViewInit() {
    this.chart = this.AmCharts.makeChart(`chartdiv${this.iteration}`, this.chartData);
  }

  ngOnDestroy() {
    if (this.chart) {
      this.AmCharts.destroyChart(this.chart);
    }
  }

  ngOnChanges() {
    // data changed, need to rerender this chart
    console.log('data changed', this.chartData);
    if (this.chart) {
      this.chart.validateData();
    }
  }
}

import { AfterViewInit, ChangeDetectorRef, Component, HostListener, OnDestroy } from '@angular/core';
import { AmChartsService, AmChart } from '@amcharts/amcharts3-angular';
import { ChartService } from './charts/charts.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  public limit = 2;
  constructor(public chartData: ChartService, private cdRef: ChangeDetectorRef) {}

  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event) {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
      this.limit++;
    }
  }
  generateCharts(amount: number): void {
    this.limit = 2;
    this.chartData.setNumberOfChartsToGenerate(amount);
  }

  changeChartData(value: number, iteration: number) {
    this.chartData.changeFirstChartData(value, iteration);
    this.cdRef.detectChanges();
  }
  // not using infinite scroll library anymore, leaving for reference
  // update($event: ILEvent) {
  //   this.event = $event;
  //   console.log($event);
  //   // $event.subscribe(x => {
  //   //     this.event = x;
  //   //   	this.cdRef.detectChanges();
  //   // });
  // }
}
